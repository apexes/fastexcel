package edu.npu.fastexcel;
/*
 * 源文件名	: XlsTest.java
 * 创建日期	: 2010-11-30 9:57:46
 *
 */

import edu.npu.fastexcel.BIFFSetting;
import edu.npu.fastexcel.FastExcel;
import edu.npu.fastexcel.Sheet;
import edu.npu.fastexcel.Workbook;
import java.io.File;

/**
 *
 * @author HeDYn<hedyn@foxmail.com>
 * @version 0.1.
 */
public class XlsTest {

    public static String toBinaryString(double d, int len) {
        StringBuilder builder = new StringBuilder();
        return Double.toHexString(d);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        //testStreamWrite();
        //*

        Workbook workbook;
        workbook = FastExcel.createReadableWorkbook(new File("test.xls"));
        workbook.setSSTType(BIFFSetting.SST_TYPE_DEFAULT);
        workbook.open();
        Sheet sheet;
        sheet = workbook.getSheet(0);
        System.out.println("SHEET:" + sheet);
        for (int i = sheet.getFirstRow(); i <= sheet.getLastRow(); i++) {
            System.out.print(i + "#");
            for (int j = sheet.getFirstColumn(); j <= sheet.getLastColumn(); j++) {
                String s = sheet.getCell(i, j);
                System.out.print("  " + s);
            }
            System.out.println();
        }
        workbook.close();
        //*/
    }

    public static void testStreamWrite() throws Exception {
        File f = new File("test_write.xls");
        Workbook wb = FastExcel.createWriteableWorkbook(f);
        wb.open();
        Sheet sheet = wb.addSheet("Sheet1");
        sheet.setCell(0, 0, "aaa");
        sheet.setCell(0, 1, "bbb");
        //Sheet sheet = wb.addStreamSheet("Sheet1");
        //sheet.addRow(new String[]{"aaa", "bbb"});
        wb.close();
    }
}
